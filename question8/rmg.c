#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <grp.h>

void rmg(char *filename){
    
    int rm = remove(filename);
    if (rm == 0)
        printf("file removed");
    else
        printf("cannot be removed");
}


int check_grp(char * filename, gid_t iduser){
    struct stat stats; 
    if(stat(filename,&stats) == -1){
        printf("cannot get the group\n");
        exit(EXIT_FAILURE);
    }

    if(iduser == stats.st_gid)
        return 0;
    else
        return -1;

}

int check_password(char *passw, char *user){
    FILE *file = NULL;
    char username[255];
    char password[255];

    file = fopen("../home/admin/passwd","w");

    if (file == NULL){
        printf("cannot open file");
        exit(EXIT_FAILURE);
    }

    while(fgets(username,255,file)){
        username[strcspn(username,"\n")] = 0;

        if(strcmp(username,user)==0){
            fgets(password,255,file);
            password[strcspn(password,"\n")] = 0;
            if(strcmp(password,passw)==0){
                return 0;
            }
        }
        printf("wrong password");

        fclose(file);
        exit(EXIT_FAILURE);

    }

}



int main(int argc, char *argv[]){
    if(argc < 1)
        printf("missing args");

    char *username = getenv("USER");
    char password[50];
    printf("check group: %d",check_grp(argv[1],getgid()));
    if(check_grp(argv[1],getgid()) == 0){
        printf("Enter your password: ");
        fgets(password,50,stdin);
        password[strcspn(password,"\n")] = 0;
    

        if(check_password(password,username))
            rmg(argv[1]);
        else
            printf("wrong password");
     
    }else{
        printf("Permission denied");
    }
}















