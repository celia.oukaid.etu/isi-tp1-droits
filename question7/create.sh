#!/bin/bash

#créer user admin
groupadd admin
sudo adduser admin --ingroup admin

#créer user lambda_a du groupe_a
groupadd groupe_a
sudo adduser lambda_a --ingroup groupe_a

#créer le répertoire pour le groupe_a
mkdir dir_a
sudo chown admin:groupe_a dir_a

#créer user lambda_b du groupe_b
groupadd groupe_b
sudo adduser lambda_b --ingroup groupe_b

#créer le répertoire pour le groupe_a
mkdir dir_b
sudo chown admin:groupe_b dir_b

mkdir dir_c

