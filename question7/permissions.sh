#!/bin/bash

sudo chmod -R g+rwx dir_a
sudo chmod -R g+rwx dir_b


sudo chmod o-rwx dir_b dir_a
sudo chmod o+rx dir_c
sudo chmod o-w dir_c
sudo chmod u+rwx dir_c
sudo chmod g+t dir_a dir_b dir_c

sudo setfacl -m u:admin:rwx dir_c

sudo setfacl -m g:groupe_a:r dir_c
sudo setfacl -m g:groupe_b:r dir_c


