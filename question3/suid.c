#include <stdlib.h>
#include <stdio.h>
#include<unistd.h>

int main(int argc, char *argv[])
{

    printf("Get a real user ID:%d\n", getuid());
    printf("Get the effective user ID:%d\n", geteuid());
    printf("Get the real group ID:%d\n", getgid());
    printf("Get the effective group ID:%d\n", getegid());

    FILE*f;
    char c;

    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }
    f = fopen(argv[1], "r");
    if (f == NULL) {
    perror("Cannot open file\n");
    exit(EXIT_FAILURE);
    }
    printf("File opens correctly\n");

    while((c=fgetc(f))!=EOF){
            printf("%c",c);
    }


    fclose(f);
    exit(EXIT_SUCCESS);
    return 0;
}

