# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Oukaid, Celia, email: celia.oukaid.etu@univ-lille.fr  

- Ferchichi, Mehdi, email: mehdi.ferchichi.etu@univ-lille.fr  

## Question 1

-r--rw-r-- 1 toto ubuntu 5 Jan 5 09:35 titi.txt  
L'utilisateur toto a seulement les droits d'accès pour la lecture (r) du fichier titi.txt car ubuntu regarde en priorité les droits du propriétaire du fichier (les 3 premiers caractères : r--) qui est toto

## Question 2

- Un répertoire doit être exécutable pour pouvoir accéder à l'ensemble de ses sous-répertoires.  
- toto reçoit le message "permission denied" s'il souhaite entrer dans le répertoire mydir car :
**drwxrw-r-x 2 ubuntu ubuntu 4096 Jan 12 16:23 mydir**  
ses droits sont rw- ubuntu étant le propriétaire de ce répertoire, on va prendre les prochains 3 caractères après ceux de ubuntu et ceux-ci ne donnent pas droit d'exécution pour le répertoire mydir ainsi il ne peut pas accéder à ses sous répertoires.  
- ce que la commande ls -al mydir renvoie
```
		ls: cannot access 'mydir/.': Permission denied
		ls: cannot access 'mydir/..': Permission denied
		ls: cannot access 'mydir/data.txt': Permission denied
		total 0
		d????????? ? ? ? ?            ? .
		d????????? ? ? ? ?            ? ..
		-????????? ? ? ? ?            ? data.txt
```
on remarque que toto peut voir ce que le répertoire mydir contient (seulement les noms) mais n'a pas d'accès et ne connait rien d'autre (pas même les droits d'accès)

## Question 3

- les différents ids pour l'utilisateur toto sont : 
```
		Real user ID: 1001
		Effective user ID: 1001
		Real group ID: 1000
		Effective group ID: 1000

```

Le processus avec l'utilisateur ne réussit pas à ouvrir le fichier mydir/data.txt et renvoie permission denied 


```
		toto@isivm:/home/ubuntu$ ./suid mydir/data.txt 
		Get a real user ID:1001
		Get the effective user ID:1000
		Get the real group ID:1000
		Get the effective group ID:1000
		File opens correctly
		lalalalala

```

On peut remarquer que toto garde son id d'utilisateur 1001 mais lance l'exécutable en tant qu'ubuntu dont l'id est 1000. Donc ubuntu étant le propriétaire, il peut ouvrir et lire le fichier avec l'éxécutable mais pas en dehors.  
  
## Question 4

```
	Get the real group ID:1001
	Get the effective group ID:1000	

```

## Question 5

- La commande chfn sert à modifier les informations personnelles (fullname, home Number,work phone,Home phone,other)  

```
	-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn

```
l'administrateur a les droits pour lire, ecrire et donne le droit à chaque utilisateur d'exécuter la commande chfn uniquement pour eux-même la commande /usr/bin/chfn  
le groupe auquel appartient l'administrateur a les droits pour lire et executer la commande /usr/bin/chfn  
les autres utilisateurs ont les droits pour lire et executer la commande /usr/bin/chfn  

- Avant d'utiliser la commande chfn nous avons ces informations dans le fichier /etc/passwd: 

```
	...
	toto:x:1001:1000:toto,,,:/home/toto:/bin/bash
```
  

Avec toto on utilise la commande chfn 
```
Enter the new value, or press ENTER for the default
	Full Name: toto
	Room Number []: 12
	Work Phone []: 0123456789
	Home Phone []: 2563987410

```

et on peut voir que dans le fichier /etc/passwd les données ont bien été modifées :

```
	...
	toto:x:1001:1000:toto,12,0123456789,2563987410:/home/toto:/bin/bash
```

## Question 6

Les mots de passe des utilisateurs sont stockés dans le fichier **/etc/shadow** car ce fichier a des permissions bien plus restreintes que /etc/passwd pour une meilleure sécurité: 

```
		-rw-r----- 1 root shadow 1156 Jan 12 15:47 /etc/shadow
```
Seul l'administrateur a les droits pour ecrire dans ce fichier et ecrire. 
Les membres du groupe shadow ont le droits de le lire 
Tous les autres utilisateurs n'ont aucun accès à ce dernier.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








